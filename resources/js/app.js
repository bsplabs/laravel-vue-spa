import "./bootstrap";

import { createApp } from "vue";

import PostsIndex from "./components/Posts/Index.vue";

const app = createApp({});

app.component("PostsIndex", PostsIndex);

app.mount("#app");
